from sqlalchemy import create_engine

# Back-end HOST configuration
HOST = "127.0.0.1"
PORT = "8080"

# DB Configuration
# Create engine that hold reference to db
# Later it will be needed to use in queries
ENGINE = create_engine('sqlite:///db/zoo.db')

# Open connection to db
DB_CONN = ENGINE.connect()
