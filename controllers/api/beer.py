from sqlalchemy import update
from sqlalchemy.orm import sessionmaker
from tornado_json import schema
from tornado_json.requesthandlers import APIHandler
from tornado.web import HTTPError

from models.beer import Malt
from settings.settings import ENGINE

SESSION = sessionmaker(bind=Malt)


class BeerApi(APIHandler):
    __url_names__ = ['beer']


class BeersApi(APIHandler):
    __url_names__ = ['beers']
