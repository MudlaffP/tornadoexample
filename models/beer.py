from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Enum

Base = declarative_base()


class Malt(Base):
    __tablename__ = 'malt'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(60), index=True, nullable=False)
    type = Column(Enum('Base', 'Caramel', 'Burnt', name='malt_type'), nullable=False)
    description = Column(String(500), nullable=False)

    def __repr__(self):
        return "Malt: {}, type: {}".format(
            self.name, self.type
        )
