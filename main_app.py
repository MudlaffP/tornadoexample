import tornado.ioloop
from tornado_json.application import Application
from tornado_json.routes import get_routes

from models.beer import Base
from settings import settings


def make_app():
    from controllers import api

    # Init database structure
    Base.metadata.create_all(settings.ENGINE)

    # Need to read about database migration in SQLAlchemy

    routes = get_routes(api)

    application = Application(routes=routes, settings={}, generate_docs=True)
    application.listen(settings.PORT, settings.HOST)

    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    make_app()